(defproject theme03-macros "0.0.3-SNAPSHOT"
  :description "Exercices sur le thème 3 : macros Clojure"
  :dependencies [[org.clojure/clojure "1.10.0"]]
  :profiles {:dev {:dependencies [[midje "1.9.6" :exclusions [org.clojure/clojure]]]
                   :plugins [[lein-midje "3.2.1"]
                             [nightlight/lein-nightlight "2.4.0"]]}
             :midje {}})

